let mapleader = ","

" Initialize Vundle

"=========================== PLUGINS ==========================================

filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Plugin Vundle
Plugin 'gmarik/Vundle.vim'
" Plugin fugitive for git intergration
Plugin 'tpope/vim-fugitive'
" Plugin airline
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin Syntastic
Plugin 'scrooloose/syntastic'
" Plugin neocomplcache
Plugin 'Shougo/neocomplcache'
Plugin 'ekalinin/Dockerfile.vim'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'w0ng/vim-hybrid'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'tpope/vim-surround'

call vundle#end()
filetype plugin on
filetype indent on
"==============================================================================
" Color scheme
set background=dark
let g:hybrid_use_Xresources = 1
let g:hybrid_custom_term_colors = 1
colorscheme hybrid
let g:airline_theme='badwolf'

" Set numbers based on current line
set relativenumber
set number
" Always show status line
" Enable syntax auto depending on file extensions
syntax on

" PostgreSQL default sql coloration
let g:sql_type_default = 'pgsql'

" Force encoding to utf-8, for systems where this is not the default (windows
" " comes to mind)
set encoding=utf-8

" Briefly show brackets matches
set showmatch

" Ignore case on search
set ignorecase

" Highlight all matches
set hlsearch
" Toggle g option by default on substition
set gdefault

" Hide the cursor when typing
set mousehide

" Fix insert mode backspace
set backspace=2

set wrap
set tw=80

filetype plugin indent on

" Indent
set tabstop=2
set smarttab
set expandtab
set shiftwidth=2
set softtabstop=2
set autoindent
set smartindent
set copyindent

" Map : on ;
noremap ; :

" Move between rows in wrapped lines
noremap j gj
noremap k gk

" Yank from cursor to EOL
nnoremap Y y$

" Minimal amount of lines under and above the cursor
set scrolloff=5
set colorcolumn=80

" Markdown
au BufNewFile,BufRead *.md setf markdown

" No swap file
set nobackup
set noswapfile

" Highlight current line
set cursorline

" Print end of lines and tab char
set list
set list listchars=tab:>-,trail:·

set noesckeys

set nocompatible

set ttimeoutlen=10

" Auto include gates for headers files
function! s:insert_gates()
  let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
  execute "normal! i#ifndef " . gatename
  execute "normal! o#define " . gatename
  execute "normal! o"
  execute "normal! Go#endif /* !" . gatename . " */"
  normal! kk
endfunction
autocmd BufNewFile *.{h,hpp,hh} call <SID>insert_gates()


" Set the C and C++ compilers used for syntax checking with syntastic
let g:syntastic_cpp_compiler='clang++'
let g:syntastic_cpp_compiler_options = '-std=c++1y -pedantic'
let g:syntastic_c_compiler='clang'
let g:syntastic_c_check_header=1
let g:syntastic_cpp_check_header=1
let g:syntastic_python_checkers=['flake8']
let g:syntastic_python_flake8_args='--ignore=E501,E225,F401,F821,E265'

" Neocomplete
let g:neocomplcache_enable_at_startup = 1
if !exists('g:neocomplcache_force_omni_patterns')
     let g:neocomplcache_force_omni_patterns = {}
endif
let g:neocomplcache_force_omni_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
let g:neocomplcache_force_omni_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3

let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

autocmd filetype python set nowrap textwidth=0

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

set laststatus=2

" air-line
let g:airline_powerline_fonts = 1

" if !exists('g:airline_symbols')
"     let g:airline_symbols = {}
" endif
" 
" " unicode symbols
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'ρ'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'
" let g:airline_symbols.whitespace = 'Ξ'
" 
" " airline symbols
" let g:airline_left_sep = ''
" let g:airline_left_alt_sep = ''
" let g:airline_right_sep = ''
" let g:airline_right_alt_sep = ''
" let g:airline_symbols.branch = ''
" let g:airline_symbols.readonly = ''
" let g:airline_symbols.linenr = ''
