# README

## i3

- i3-gaps
- i3blocks
- multimedia keys
- workspace icons
- Some useful shortcurs

## Vim

- Plugins
- Base16 colorscheme
- Clean pasting from clipboard without :set paste

## Zsh

- Syntax highlighting
- Autojump
- A few aliases

## X

- Nice and smooth animations with compton
- Xresources
- Autocutsel
