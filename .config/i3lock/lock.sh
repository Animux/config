
#!/bin/bash

xdpyinfo -ext XINERAMA | sed '/^  head #/!d;s///' |
while IFS=' :x@,' read i w h x y; do
    import -window root -crop ${w}x$h+$x+$y /tmp/head_$i.png
done
convert /tmp/head_0.png -scale 10% -scale 1000% /tmp/head_0.png
[[ -f ~/Images/lock-image.png ]] && convert /tmp/head_0.png ~/Images/lock-image.png -gravity center -composite -matte /tmp/head_0.png
if [ -e /tmp/head_1.png ]; then
  convert /tmp/head_1.png -scale 10% -scale 1000% /tmp/head_1.png
  [[ -f ~/Images/lock-image.png ]] && convert /tmp/head_1.png ~/Images/lock-image.png -gravity center -composite -matte /tmp/head_1.png
  convert /tmp/head_0.png /tmp/head_1.png +append /tmp/screen.png
else
  mv /tmp/head_0.png /tmp/screen.png
fi
i3lock -u -i /tmp/screen.png
