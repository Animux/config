# Set up the prompt

export TERM="xterm-256color"

autoload -Uz promptinit
promptinit

source ~/antigen.zsh

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle autojump
antigen bundle lein
antigen bundle command-not-found

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Load the theme.
antigen theme gentoo

# Tell Antigen that you're done.
antigen apply


setopt histignorealldups sharehistory

# # Use emacs keybindings even if our EDITOR is set to vi
# bindkey -e
# bindkey    "^[[3~"          delete-char

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit && compinit -i

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'



alias sx='exec startx; exit'
alias ls='ls --color=auto'
alias ll='ls -lash'
alias vimrc='vim ~/.vimrc'
alias zshrc='vim ~/.zshrc'

export PAGER=most
export EDITOR=vim

alias g="git"
alias pg='ping 8.8.8.8'
alias fr='setxkbmap fr'

source $HOME/.zsh_aliases
# source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# source /usr/share/autojump/autojump.sh

export PATH=$PATH:/root/packages/firefox/
export PATH=$PATH:/root/packages/google-cloud-sdk/bin/
fpath=(~/.zsh/completion $fpath)


#  BASE16_SHELL=$HOME/.config/base16-shell/
# [ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/root/packages/google-cloud-sdk/path.zsh.inc' ]; then source '/root/packages/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/root/packages/google-cloud-sdk/completion.zsh.inc' ]; then source '/root/packages/google-cloud-sdk/completion.zsh.inc'; fi
